package com.unus.sakuservice.core;
import javax.persistence.*;

@Entity
@Table(name = "saku")
public class SakuBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(columnDefinition = "TEXT", name="description")
    private String desc;

    @Column(nullable = false)
    private long userid;

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String desc) {
        this.desc = desc;
    }

    public void setUser(Long id_user) {
        this.userid = id_user;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return desc;
    }

    public long getUser() {
        return userid;
    }
}
