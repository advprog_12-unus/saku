package com.unus.sakuservice.service;

import com.unus.sakuservice.core.SakuBase;

import java.util.List;

public interface SakuService {
    public void deleteSaku(Long id); 

    public SakuBase register(SakuBase saku);

    public List<SakuBase> getAllSakuOfUser(Long id_user);

}
