package com.unus.sakuservice.service;

import com.unus.sakuservice.core.SakuBase;
import com.unus.sakuservice.repository.SakuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SakuServiceImpl implements SakuService {
    @Autowired
    private SakuRepository sakuRepo;

    public SakuServiceImpl(SakuRepository sakuRepo) {
        this.sakuRepo = sakuRepo;
    }

    @Override
    public SakuBase register(SakuBase saku) {
        System.out.println("sini\n");
        return sakuRepo.save(saku);
    }

    @Override
    public void deleteSaku(Long id) {
        sakuRepo.deleteById(id);
    }

    @Override
    public List<SakuBase> getAllSakuOfUser(Long id_user) {
        System.out.println("situ\n");
        System.out.println(id_user);
        return sakuRepo.findAllByUserid(id_user);
    }
}
