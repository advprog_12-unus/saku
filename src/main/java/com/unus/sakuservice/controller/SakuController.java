package com.unus.sakuservice.controller;

import com.unus.sakuservice.core.SakuBase;
import com.unus.sakuservice.repository.SakuRepository;
import com.unus.sakuservice.service.SakuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@RestController
@RequestMapping(value = "/service-saku")
public class SakuController {

    @Autowired
    private SakuService sakuService;

    public SakuController(SakuService sakuService) {
        this.sakuService = sakuService;
    }

    @GetMapping("/allSaku/{id_user}")
    private List<SakuBase> dapat(@PathVariable("id_user") Long id_user){
        System.out.println("get\n");
        return sakuService.getAllSakuOfUser(id_user);
    }

    @GetMapping("/create-saku")
    public SakuBase createSaku(Model model) {
        return new SakuBase();
    }

    @PostMapping("/add-saku/{id_user}")
    public void addSaku(@PathVariable("id_user") Long id_user, @RequestBody SakuBase saku) {
        System.out.println("add\n");
        saku.setUser(id_user);
        sakuService.register(saku);
    }

    @GetMapping("/delete-saku/{id}")
    public void deleteSaku(@PathVariable Long id) {
        sakuService.deleteSaku(id);
    }
}
