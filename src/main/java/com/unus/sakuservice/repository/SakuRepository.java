package com.unus.sakuservice.repository;

import com.unus.sakuservice.core.SakuBase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SakuRepository extends JpaRepository<SakuBase, Long> {
    List<SakuBase> findAllByUserid(Long userid);
}
