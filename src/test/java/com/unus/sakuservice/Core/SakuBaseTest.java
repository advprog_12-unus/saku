package com.unus.dompettest.core;
import org.junit.jupiter.api.BeforeEach;

import com.unus.sakuservice.core.SakuBase;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class SakuBaseTest {

    private SakuBase saku;

    @BeforeEach
    void setUp() {
        saku = new SakuBase();
        saku.setName("Saku");
        saku.setDescription("DescTest");
        saku.setUser((long)1);
    }

    @Test
    public void setUserTest() {
        saku.setUser((long)2);
        assertEquals(2, saku.getUser());
        saku.setUser((long)1);
    }

    @Test
    public void getUserTest() {
        assertEquals(1, saku.getUser());
    }

    @Test
    void testobject1() {
        assertEquals("Saku", saku.getName());
    }

    @Test
    void testobject2() {
        assertEquals("DescTest", saku.getDescription());
    }

    @Test
    void getIdTest() {
        assertEquals(0, saku.getId());
    }

    @Test
    void getNameTest() {
        assertEquals("Saku", saku.getName());
    }

    @Test
    void getDescTest() {
        assertEquals("DescTest", saku.getDescription());
    }
}