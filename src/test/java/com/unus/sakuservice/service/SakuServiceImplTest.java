package com.unus.sakuservice.service;

import com.unus.sakuservice.core.SakuBase;
import com.unus.sakuservice.repository.SakuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class SakuServiceImplTest {
    @Mock
    private SakuRepository sakuRepo;

    private SakuBase sakuTest;

    @InjectMocks
    private SakuServiceImpl sakuService;

    @BeforeEach
    void setUp() {
        sakuTest = new SakuBase();
        sakuTest.setName("TestingTitle");
        sakuTest.setDescription("TestingDescription");

    }

    

    @Test
    void registerTest() {
        sakuService.register(sakuTest);
        lenient().when(sakuService.register(sakuTest)).thenReturn(sakuTest);
    }

    @Test
    void getAllSakuOfUserTest() {
        List<SakuBase> sakuList = sakuService.getAllSakuOfUser((long)1);
        lenient().when(sakuService.getAllSakuOfUser((long)1)).thenReturn(sakuList);
    }

    @Test
    void deleteSakuTest() {
        sakuService.register(sakuTest);
        sakuService.deleteSaku(sakuTest.getId());
        lenient().when(sakuService.register(sakuTest)).thenReturn(sakuTest);
    }
}