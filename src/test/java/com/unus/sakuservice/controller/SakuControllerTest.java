package com.unus.sakuservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unus.sakuservice.core.SakuBase;
import com.unus.sakuservice.repository.SakuRepository;
import com.unus.sakuservice.service.SakuServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = SakuController.class)
class SakuControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SakuRepository sakuRepository;

    @MockBean
    private SakuServiceImpl sakuService;

    @Test
    private void allSakuTest() throws Exception {
        mockMvc.perform(get("/service-saku/allSaku/1}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(new ResultHandler() {
                    @Override
                    public void handle(MvcResult result) throws Exception {
                        assertEquals("[]",result.getResponse().getContentAsString());
                    }
                });
    }

    @Test
    void createSakuTest() throws Exception {
        SakuBase sakuTest = new SakuBase();
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(sakuTest);

        mockMvc.perform(post("/service-saku/create-saku").content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void addSaku() throws Exception {
        SakuBase sakuTest = new SakuBase();
        sakuService.register(sakuTest);
        lenient().when(sakuService.register(sakuTest)).thenReturn(sakuTest);
        mockMvc.perform(get("/service-saku/add-saku/1"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void deleteSaku() throws Exception {
        mockMvc.perform(delete("/service-saku/delete-saku/1"))
                .andExpect(status().is4xxClientError());
    }
}