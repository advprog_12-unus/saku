# Saku Service

[![pipeline status](https://gitlab.com/advprog_12-unus/saku/badges/master/pipeline.svg)](https://gitlab.com/advprog_12-unus/saku/-/commits/master)
[![coverage report](https://gitlab.com/advprog_12-unus/saku/badges/master/coverage.svg)](https://gitlab.com/advprog_12-unus/saku/-/commits/master)

Hanya microservice dan harap dijalankan bersama registry
sebelum membuka main service

Deployed on : http://unus-saku.herokuapp.com/